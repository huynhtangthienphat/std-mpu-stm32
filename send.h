/*
	Written by Huynh Tang Thien Phat
	date 19/2/2024
*/

#ifndef __SEND_H
#define __SEND_H

#include "stm32f10x.h"                  // Device header
#include <stdio.h>
/*
	define for uart
*/
#define GPIO_CRH_ALT9 ((uint32_t)0x000000B0) 
#define GPIO_CRH_INPUT10 ((uint32_t)0x00000800) 

void SetSystem72Mhz(void);
void usart1_sendByte(unsigned char c);
void SendKyTu(const char str[100],char str2[]);
void sendNumber(const char str[100],int data);
void sendNumberAndChu(const char str[100],char str2[],int data);
void sendDoubleAndChu(const char str[100],char str2[],double data);
void sendFloatAndChu(const char str[100],char str2[],float data);
#endif
