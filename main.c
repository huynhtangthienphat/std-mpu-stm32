#include "stm32f10x.h"                  // Device header
#include <stdio.h>
#include "delay.h"
#include "mpu.h"
#include "MadgwickAHRS.h"

#include "send.h"
//extern float mpu6050_gf[3];
//extern float mpu6050_af[3];
//uint8_t data[5];
double Yaw_m,Pitch_m,Roll_m;


int main(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN; //enable GPIO clocks
	GPIOA->CRH |= GPIO_CRH_ALT9|GPIO_CRH_INPUT10;
	GPIOA->ODR |= GPIO_ODR_ODR10;  //pull-up PA10
	//GPIOA->CRH = 0x444448B4; // RX1=input with pull-up, TX1=alt. func output
	USART1->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	USART1->BRR = 625;	 // 72MHz/9600bps = 7500
	char buffer[1000];
	//float i=0,a=0,b=0,c=0;
	delay_init();
	// chien
	MPU6050_I2C_Init();
	MPU6050_Initialize();
	
	MPU6050_SetFullScaleGyroRange(MPU6050_GYRO_FS_250);
	MPU6050_SetFullScaleAccelRange(MPU6050_ACCEL_FS_2);
	MPU6050_SetClockSource(MPU6050_CLOCK_PLL_XGYRO);
	
	if(MPU6050_TestConnection())
	{
		SendKyTu(buffer,"ket noi thanh cong");
	}
	if(MPU6050_GetSleepModeStatus())
	{
		SendKyTu(buffer,"da ngu");
		DelayMs(1000);
	}
	else
	{
		SendKyTu(buffer,"da thuc");
		DelayMs(1000);
		int deviceid=(int)MPU6050_GetDeviceID();
		
		sendNumberAndChu(buffer,"id cua thiet bi la: ",deviceid);
		DelayMs(1000);
	}
	double giatoc[3];
	double gocQuay[3];
	int16_t check[6];
	while(1)
	{
		MPU6050_GetRawAccelGyro(check);
		giatoc[0]=(double)check[0]; // gia toc
		giatoc[1]=(double)check[1]; // gia toc
		giatoc[2]=(double)check[2]; // gia toc
		gocQuay[0]=(double)check[3]; // goc quay 
		gocQuay[1]=(double)check[4]; // goc quay 
		gocQuay[2]=(double)check[5]; // goc quay 
		// convert float
		//MadgwickAHRSupdateIMU(gocQuay[0],gocQuay[1],gocQuay[2],giatoc[0],giatoc[1],giatoc[2]);
	//	Pitch_m=100*(-asin (2*(q1*q3 - q0*q2)));
//	  Yaw_m=100* atan2(2 * q1 * q2 + 2 * q0 * q3,2 * q0 * q0 + 2 * q1 * q1-1);
//	  Roll_m=100*atan2(2 * q2 * q3 + 2 * q0 * q1, 2 * q0 * q0 + 2 * q3 * q3 - 1);
		sendFloatAndChu(buffer,"data :",(float)gocQuay[2]);
		
		DelayMs(10);
	}
}

