#ifndef __HAL_MPU_H
#define __HAL_MPU_H

#include "stm32f10x.h"                  // Device header
#include <stdbool.h>
#define MPU6050_I2C I2C2
#define MPU6050_I2C_RCC_Periph RCC_APB1Periph_I2C2
#define MPU6050_I2C_Port	GPIOB
#define MPU6050_I2C_SCL_Pin 	GPIO_Pin_10
#define MPU6050_I2C_SDA_Pin 	GPIO_Pin_11
#define MPU6050_I2C_RCC_Port	RCC_APB2Periph_GPIOB
#define MPU6050_I2C_Speed	100000 // che mode standard



#endif